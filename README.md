# vlf_movement

This complements a manuscript on audience response to a very-low frequency (VLF) stimulus during a naturalistic study at the LIVELab, McMaster University. The first part is pre-processing, done in Matlab, and includes everything needed to get from the raw mocap data, as exported from a 24-camera Qualisys system, to the dependent variable, speed. A raw data file with two participants is included as well. For the full dataset, contact the authors.
