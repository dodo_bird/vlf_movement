function [D,sf,IDS_num,IDS_str] = import_orphx_set(filename_raw_data,filename_markers_ids,plotting_flag)

% The header of the text file.
delimiter = '\t';
fileID = fopen(filename_raw_data,'r');
paramArray = textscan(fileID, '%s%f', 6, 'Delimiter', delimiter, 'EmptyValue' ,NaN,'HeaderLines', 0, 'ReturnOnError', false, 'EndOfLine', '\r\n');
for n=1:6
    if strcmp(paramArray{1,1}{n},'NO_OF_FRAMES')
        nrows=paramArray{1,2}(n);
    end
    if strcmp(paramArray{1,1}{n},'NO_OF_MARKERS')
        nchannels=paramArray{1,2}(n);
    end
    if strcmp(paramArray{1,1}{n},'FREQUENCY')
        sf=paramArray{1,2}(n);
    end
end

% Read the marker labels.
col_names = textscan(fileID, '%s', nchannels+1, 'Delimiter', delimiter, 'EmptyValue' ,NaN,'HeaderLines', 4, 'ReturnOnError', false, 'EndOfLine', '\r\n');
col_names = str2double(col_names{1}(2:end));

% Pull the raw numeric data. Then reshape the array = {number of samples x three dimensions x number of markers}.
dataArray = textscan(fileID, '%f', 'Delimiter', delimiter, 'EmptyValue' ,NaN, 'HeaderLines', 1, 'ReturnOnError', false, 'EndOfLine', '\r\n');
fclose(fileID);
D=reshape(dataArray{1},nchannels*3,nrows)';
D=reshape(D,[],3,nchannels);
clear dataArray

% Check that we're getting the right number of individual markers.
if size(D,3)~=numel(col_names)
    fprint('F!\n')
    keyboard
end

% Visually inspect the whole audience in a 2D view from above.
if plotting_flag == 1
    figure
    subplot(1,2,1)
    for pp=1:size(D,3)
        plot(D(1:1e2:end,1,pp),D(1:1e2:end,2,pp));
        text(nanmedian(D(1:1e2:end,1,pp)),nanmedian(D(1:1e2:end,2,pp)),num2str(col_names(pp),'%.0f'))
        hold on
    end
    grid on
    hold off
    xlabel('X, mm [AP of the floor]')
    ylabel('y, mm [ML of the floor]')
end

% IDs. These were alphabet chars that identified participants and linked
% them anonymously to other modalities of measurement such as questionnaires.
[IDS_str,IDS_num] = import_subj_marker_labels(filename_markers_ids);

% Merge redundant 3D traces and multiple markers on the same headband.
% Markers not associated with an alphabetic label are not retained. There 
% are a couple of these due to the participant staying outside the mocap 
% field of view and/or the participant leaving early.
[D,IDS_str,IDS_num] = crop_D_and_labels(D,col_names,IDS_str,IDS_num); 

% Compare participant labels and marker numbers with those in Subj_Markers.txt
fprintf('%15s%20s\n','Participant ID','Mocap markers #')
for r=1:numel(IDS_str)
    fprintf('%15s%4.0f\n',IDS_str{r},IDS_num(r))
end

if plotting_flag == 1
    subplot(1,2,2)
    for pp=1:size(D,3)
        plot(D(1:1e2:end,1,pp),D(1:1e2:end,2,pp));
        text(nanmedian(D(1:1e2:end,1,pp)),nanmedian(D(1:1e2:end,2,pp)),IDS_str{pp})
        hold on
    end
    grid on
    hold off
    xlabel('X, mm [AP of the floor]')
    ylabel('Y, mm [ML of the floor]')
end

end


%%
function [D_merged,IDS_str_merged,IDS_num_merged] = crop_D_and_labels(D,col_names,IDS_str,IDS_num)

IDS_num_merged = zeros(size(IDS_str));

D_merged = zeros(size(D,1),3,size(IDS_num,2));
for n = 1:size(IDS_num,2)
    [r,~] = find(col_names==IDS_num{n});
    % We merge redundant 3D traces by nan-summing. This assumes that the 
    % markers come clean out of Qualisys and that indexed markers that stand 
    % for the same physical marker will complement each other w/out overlapping. 
    % Empty parts are nans.
    D_merged(:,:,n) = nanmean(D(:,:,r),3);
    for d=1:3
        D_merged(D_merged(:,d,n)==0,:,n) = nan;
    end
    IDS_num_merged(n) = col_names(r(1));
end

% Find the redundant vectors on the same head. There could be between 1 and
% 3 physical markers on the same headband. We pick the one with the least
% number of missing samples.
remove_markers = [];
for n = 2:size(IDS_num,2)
    % Assume that labels and matrices are ordered, so only seek contiguous repetitions.
    if strcmp(IDS_str{n-1},IDS_str{n})
        % Remove the marker with more nans.
        if sum(isnan(D_merged(:,1,n-1))) > sum(isnan(D_merged(:,1,n)))
            remove_markers = horzcat(remove_markers, n-1);
        else
            remove_markers = horzcat(remove_markers, n);
        end
    end
end

retained_markers = find(all((((1:numel(IDS_str))'==remove_markers)==0)'));
D_merged = D_merged(:,:,retained_markers);
IDS_num_merged = IDS_num_merged(retained_markers);
IDS_num_merged = floor(IDS_num_merged./10);
IDS_str_merged = IDS_str(retained_markers);

end


%%
function [IDS_str,IDS_num] = import_subj_marker_labels(filename)

delimiter = ',';
fileID = fopen(filename, 'r');
x = textscan(fileID, '%s', 'Delimiter', delimiter, 'EmptyValue', NaN, 'HeaderLines', 0, 'ReturnOnError', false, 'EndOfLine', '\r\n');
fclose(fileID);

IDS_str = cell(1,1);
IDS_num = cell(1,1);
c = 0;
for n = 1:numel(x{1})
    if isnan(str2double(x{1}{n}))
        c = c+1;
        IDS_str{c} = x{1}{n};
        IDS_num{c} = [];
    else
        IDS_num{c} = [IDS_num{c} str2double(x{1}{n})];
    end
end
end