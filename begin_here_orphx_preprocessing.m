% Dobri Dotov, Feb 23 2022, Hamilton, Ontario

% Only analyze a small representative sample of participants.
subset_of_pps = 1;

% Add the folder with .m files to the path.
script_path = input('Where are the scripts?\n');
addpath(script_path)

% Where is the raw data stored?
% data_path = input('Where is my data?\n');
data_path = script_path;

if subset_of_pps == 1
    filename_raw_data = fullfile(data_path,'orphx_set_selected_pp.tsv');
    filename_markers_ids = 'Subj_Markers_selected.txt'; % This can be with the scripts.
else
    filename_raw_data = fullfile(data_path,'orphx_set.tsv');
    filename_markers_ids = 'Subj_Markers.txt'; % This can be with the scripts.
end

% Set this to visually inspect the multiple stages of data processing.
plotting_flag = 1;

% This will import the text file with raw recording of head marker positions.
% The participant labels (IDs) are also imported and matched to the 3D data.
% D_RAW is an array (time step x dimension x marker) of coordinates in the
% frame of refence of the room. The entire data collection session, one set
% of the concert, is contained here and will be broken into trials later.
% If plotting_flag==1 then the participants' movement around the room will be shown.
[D_RAW,sf,IDS_num,IDS_str] = import_orphx_set(filename_raw_data,filename_markers_ids,plotting_flag);


%% Remove gross movement. Then inspect visually.
% A non-parametric method smoothes the trajectory. This trajectory stands 
% for the gross body location in the room. Subtract it! We analyze what 
% remains after this subtraction, namely postural fluctuations.
% Set the last parmater to 1 to enable individual trial plotting for
% debugging purposes.
t = (1:size(D_RAW,1))'./sf; % Time
[D_FULL_CLEANED, GrossPosition] = remove_gross_movement(D_RAW,sf,t,[],[],0);

% Manually code large artifacts such as exiting the room.
if subset_of_pps == 1
    artefacts = {[],[]};
else
    artefacts = {[],[],[],[],[],[],...
        [207,213],[],[],[],[],[],...
        2923,[2725,2933],[],[733,815,3284,3285],[],[],[2506,2808],...
        [],[],[],[],[2019,2154,2523],[],[1751,2677],[323,2115],[],...
        [1088,1182,2109,2126],[19,1078],[1212,1952,2329],[],[],...
        [270,1070,1370,1437,1706,2114,2195,2209,2959],941,1654,272,...
        [],[],[],[],[],[1635,2032,2599]};
end

% Trial beginning and endpoints [seconds]
trial_cut_time_points = ...
    [1 304;305 453;454 607;608 751;752 903;904 1051;1052 1199;...
    1200 1349;1350 1499;1500 1649;1650 1800;1801 1951;1952 2099;...
    2100 2249;2250 2399;2400 2550;2551 2701;2702 3350];
vlf_switch_time_points = [trial_cut_time_points(:,1);trial_cut_time_points(end,2)];

% Clear 2 seconds before and after a marked artefact.
for pp=1:size(D_RAW,3)
    for d=1:3
        for a=1:numel(artefacts{pp})
            D_FULL_CLEANED(((artefacts{pp}(a)-2)*sf):((artefacts{pp}(a)+2)*sf),d,pp)=nan;
        end
    end
end


%% Visualize the raw and cleaned data, per participant and axis, with trials marked.
if plotting_flag == 1
    figure(3473154)
    set(gcf,'Position',[10 10 1900 1000]);
    for pp=1:size(D_RAW,3)
        for d=1:3
            fprintf('Marker #%.0f, axis %.0f.\n',pp,d)
            
            ax1 = subplot(2,3,d);
            plot(t,D_RAW(:,d,pp))
            hold on
            plot(t,GrossPosition(:,d,pp))
            hold off
            l=line([vlf_switch_time_points(:,1) vlf_switch_time_points(:,1)]',[vlf_switch_time_points(:,1) vlf_switch_time_points(:,1)]'.^0.*[nanmin(GrossPosition(:,d,pp)) nanmax(GrossPosition(:,d,pp))]','color','k','linestyle','--');
            ylabel(['X_' num2str(d)])
            title('Raw coordinates and smoothed position')
            legend(l(end),'VLF switch ON/OFF')
            
            ax2 = subplot(2,3,d+3);
            plot(t,D_FULL_CLEANED(:,d,pp))
            hold on
            for a=1:numel(artefacts{pp})
                plot(artefacts{pp}(a),nanmean(D_FULL_CLEANED(:,d,pp)),'^','markersize',10,'linewidth',3)
            end
            l=line([vlf_switch_time_points(:,1) vlf_switch_time_points(:,1)]',[vlf_switch_time_points(:,1) vlf_switch_time_points(:,1)]'.^0.*[nanmin(D_FULL_CLEANED(:,d,pp)) nanmax(D_FULL_CLEANED(:,d,pp))]','color','k','linestyle','--');
            hold off
            xlabel('Time, s')
            ylabel(['X_' num2str(d)])
            title('Head movement with gross location subtracted')
            legend(l(end),'VLF switch ON/OFF')
            
            linkaxes([ax1 ax2], 'x'); zoom xon;

        end
        pause
    end
end


%% Cut into trials.
num_trials = size(trial_cut_time_points,1);
index = trial_cut_time_points*sf;
D = cell(length(index),1);
for j = 1:length(index)
    D{j} = D_FULL_CLEANED(index(j,1):index(j,2),:,:);
end


%% Speed DV.
rez = [];
counter = 0;
for tr=1:size(D,1)
    for pp=1:size(D{tr},3)
        fprintf('Trial %2.0f, participant %2.0f.\n',tr,pp)
        
        X=D{tr}(:,:,pp);
        counter = counter + 1;
        condition = mod(tr+1,2); % VLF alternated OFF-ON from trial to trial. Convert this to Condition 0 and 1.
        rez(counter,:) = [tr pp IDS_num(pp) condition nan nan];
        % Exclusion criterion. Skip if more than .5 of the trial is missing.
        if sum(isnan(X(:,1)))/size(X,1)>.5
            continue
        end
        
        % Crop the nans. After pre-processing, there aren't many intervals 
        % with missing data and they are not mis-aligned.
        X(sum(isnan(X),2)>0,:)=[];
        
        s = dot(diff(X(300:end,:)),diff(X(300:end,:)),2).^.5./(1/sf); % speed, mm/s
        pl = sum(dot(diff(X(300:end,:)),diff(X(300:end,:)),2).^.5)/(size(X,1)/sf/60); % path length per minute
        rez(counter,5:6) = [median(s) pl];
    end
end


%% Export to a csv file.
% Then import the rez...csv file in R and run the stats in mov_stats.R.
save_csv = 0;
if save_csv == 1
    rez = array2table(rez);
    rez.Properties.VariableNames = {'Trial','pp','ID','Condition','Speed','PathLength'};
    writetable(rez,fullfile(script_path,['rez_' datestr(now,'yyyy-mm-dd') '.csv']))
end


%% That's it.
