function [D_clean,GrossPosition] = remove_gross_movement(D,sf,t,ds,smooth_param,plotting_flag)
% D_clean is nxdxk array, where n is number of samples, d is dimension (1:3), and
% k is number of markers, or participants, or whatever else is the unit.

if isempty(ds)
    ds=1e2;
end
if isempty(smooth_param)
    smooth_param = 1e-6;
end
if isempty(t)
    t = (1:size(D,1))'./sf;
end
if isempty(plotting_flag)
    plotting_flag = 1;
end


D_clean = zeros(size(D,1),size(D,2),size(D,3));
GrossPosition = nan(size(D,1),size(D,2),size(D,3));
for pp=1:size(D,3)
    
    x = D(:,:,pp);

    time_index = find(sum(isnan(x),2)==0);
    t_less_the_nans = (1:numel(time_index))';
    x = x(sum(isnan(x),2)==0,:);
    x2 = nan(size(D,1),3);
    
    gross_trend = zeros(size(x));
    for d=1:3
        fprintf('Marker #%.0f, axis %.0f.\n',pp,d)
        
        f=fit(t_less_the_nans(1:ds:end),x(1:ds:end,d),'smoothingspline','SmoothingParam',smooth_param);
        gross_trend(:,d) = feval(f,t_less_the_nans);
        
        x_less_the_nans_and_trend = x(:,d)-gross_trend(:,d);
        
        for n=1:numel(x_less_the_nans_and_trend)
            x2(time_index(n),d) = x_less_the_nans_and_trend(n);
            GrossPosition(time_index(n),:,pp) = gross_trend(n,:);
        end
        D_clean(:,:,pp) = x2;
        
        if plotting_flag == 1
            figure(13468)

            subplot(4,1,1)
            plot(t,D(:,d,pp))
            title('Raw coordinates')
            
            subplot(4,1,2)
            plot(t_less_the_nans./sf,x(:,d),t_less_the_nans./sf,gross_trend(:,d))
            title('Raw coordinates and smoothed position. Missing parts removed')
            
            subplot(4,1,3)
            plot(t,x2(:,d))
            title('Head movement with gross location subtracted')
            
            subplot(4,1,4)
            plot(t_less_the_nans./sf,x_less_the_nans_and_trend)
            xlabel('Time, s')
            title('Head movement with gross location subtracted. Missing parts removed')

            pause
        end
    end
end